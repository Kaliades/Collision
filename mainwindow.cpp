#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "physic/physicball.h"
#include "physic/vectorofmountion.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    timer = new QTimer();
    areBallsMoving = false;
    connect(ui->startStop_btn, SIGNAL(released()), this, SLOT(onStartStopBtn()));
    connect(timer, SIGNAL(timeout()), this, SLOT(startStopMoveBall()));
    connect(ui->reset_btn, SIGNAL(released()), this, SLOT(resetBtn()));
    calculator = nullptr;

}

MainWindow::~MainWindow()
{
    delete ui;
    delete scene;
    delete timer;
    if(calculator!=nullptr)
    delete  calculator;

}

void MainWindow::init()
{
    ui->graphicsView->setSceneRect(0,0,
                                   ui->graphicsView->width(),
                                   ui->graphicsView->height());
    int fDiameter = ui->firstDiameter->value();
    int sDiameter = ui->secondDiameter->value();
    scene = new MyScene(*(ui->graphicsView), fDiameter, sDiameter);
    ui->graphicsView->setScene(scene);
}

void MainWindow::onStartStopBtn()
{
    if(calculator==nullptr)
        initData();
    if(!areBallsMoving)
    {
        ui->startStop_btn->setText("Stop");
        timer->start(25);
    }else{
        timer->stop();
        ui->startStop_btn->setText("Start");
    }
    areBallsMoving = !areBallsMoving;
}

void MainWindow::startStopMoveBall()
{
    calculator->start();
}

void MainWindow::resetBtn()
{
    timer->stop();
    ui->startStop_btn->setText("Start");
    delete scene;
    delete calculator;
    calculator = nullptr;
    init();
}
void MainWindow::initData()
{
    // first ball
    double fX_Velocity = ui->firstX_Velocity->value();
    double fY_Velcity = ui->firstY_Velocity->value();
    VectorOfMountion* firstVector = new VectorOfMountion();
    firstVector->setXEquation(new VectorOfMountion::EquationOfMontion(scene->getFirstBallCurrentPostion().getX(), fX_Velocity,0));
    firstVector->setYEquation(new VectorOfMountion::EquationOfMontion(scene->getFirstBallCurrentPostion().getY(), fY_Velcity, 0));
    int fDiameter = scene->getDiameterFirstBall();
    int fMass = ui->firstMass->value();
    //second ball
     double sX_Velocity = ui->secondX_Velocity->value();
    double sY_Velcity = ui->secondY_Velocity->value();
    VectorOfMountion* secondVector = new VectorOfMountion();
    secondVector->setXEquation(new VectorOfMountion::EquationOfMontion(scene->getSecondBallCurrentPostion().getX(),sX_Velocity, 0));
    secondVector->setYEquation(new VectorOfMountion::EquationOfMontion(scene->getSecondBallCurrentPostion().getY(), sY_Velcity, 0));
    int sDiameter = scene->getDiameterSecondBall();
    int sMass = ui->secondMass->value();

    calculator = new PhysicCalculator(scene, new PhysicBall(firstVector, fDiameter, fMass), new PhysicBall(secondVector, sDiameter, sMass));

}






































