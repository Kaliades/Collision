/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QGridLayout *gridLayout;
    QDoubleSpinBox *secondX_Velocity;
    QLabel *label;
    QLabel *label_11;
    QLabel *label_9;
    QLabel *label_10;
    QSpinBox *firstMass;
    QSpinBox *secondMass;
    QDoubleSpinBox *firstX_Velocity;
    QSpinBox *secondDiameter;
    QSpinBox *firstDiameter;
    QLabel *label_5;
    QLabel *label_12;
    QLabel *label_3;
    QDoubleSpinBox *firstY_Velocity;
    QLabel *label_7;
    QDoubleSpinBox *secondY_Velocity;
    QPushButton *reset_btn;
    QPushButton *startStop_btn;
    QGraphicsView *graphicsView;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1500, 800);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        secondX_Velocity = new QDoubleSpinBox(centralWidget);
        secondX_Velocity->setObjectName(QString::fromUtf8("secondX_Velocity"));
        secondX_Velocity->setMinimum(-99.000000000000000);

        gridLayout->addWidget(secondX_Velocity, 0, 7, 1, 1);

        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout->addWidget(label_11, 0, 10, 1, 1);

        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout->addWidget(label_9, 0, 4, 1, 1);

        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout->addWidget(label_10, 1, 4, 1, 1);

        firstMass = new QSpinBox(centralWidget);
        firstMass->setObjectName(QString::fromUtf8("firstMass"));
        firstMass->setMinimum(1);

        gridLayout->addWidget(firstMass, 0, 5, 1, 1);

        secondMass = new QSpinBox(centralWidget);
        secondMass->setObjectName(QString::fromUtf8("secondMass"));
        secondMass->setMinimum(1);

        gridLayout->addWidget(secondMass, 0, 11, 1, 1);

        firstX_Velocity = new QDoubleSpinBox(centralWidget);
        firstX_Velocity->setObjectName(QString::fromUtf8("firstX_Velocity"));
        firstX_Velocity->setMinimum(-102.000000000000000);

        gridLayout->addWidget(firstX_Velocity, 0, 1, 1, 1);

        secondDiameter = new QSpinBox(centralWidget);
        secondDiameter->setObjectName(QString::fromUtf8("secondDiameter"));
        secondDiameter->setMinimum(50);
        secondDiameter->setMaximum(200);

        gridLayout->addWidget(secondDiameter, 1, 11, 1, 1);

        firstDiameter = new QSpinBox(centralWidget);
        firstDiameter->setObjectName(QString::fromUtf8("firstDiameter"));
        firstDiameter->setMinimum(50);
        firstDiameter->setMaximum(200);

        gridLayout->addWidget(firstDiameter, 1, 5, 1, 1);

        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 0, 6, 1, 1);

        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout->addWidget(label_12, 1, 10, 1, 1);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 1, 0, 1, 1);

        firstY_Velocity = new QDoubleSpinBox(centralWidget);
        firstY_Velocity->setObjectName(QString::fromUtf8("firstY_Velocity"));
        firstY_Velocity->setMinimum(-99.000000000000000);

        gridLayout->addWidget(firstY_Velocity, 1, 1, 1, 1);

        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout->addWidget(label_7, 1, 6, 1, 1);

        secondY_Velocity = new QDoubleSpinBox(centralWidget);
        secondY_Velocity->setObjectName(QString::fromUtf8("secondY_Velocity"));
        secondY_Velocity->setMinimum(-99.000000000000000);

        gridLayout->addWidget(secondY_Velocity, 1, 7, 1, 1);


        horizontalLayout->addLayout(gridLayout);

        reset_btn = new QPushButton(centralWidget);
        reset_btn->setObjectName(QString::fromUtf8("reset_btn"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(reset_btn->sizePolicy().hasHeightForWidth());
        reset_btn->setSizePolicy(sizePolicy1);
        reset_btn->setMinimumSize(QSize(150, 50));

        horizontalLayout->addWidget(reset_btn);

        startStop_btn = new QPushButton(centralWidget);
        startStop_btn->setObjectName(QString::fromUtf8("startStop_btn"));
        sizePolicy1.setHeightForWidth(startStop_btn->sizePolicy().hasHeightForWidth());
        startStop_btn->setSizePolicy(sizePolicy1);
        startStop_btn->setMinimumSize(QSize(150, 50));

        horizontalLayout->addWidget(startStop_btn);


        verticalLayout->addLayout(horizontalLayout);

        graphicsView = new QGraphicsView(centralWidget);
        graphicsView->setObjectName(QString::fromUtf8("graphicsView"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(graphicsView->sizePolicy().hasHeightForWidth());
        graphicsView->setSizePolicy(sizePolicy2);
        graphicsView->setFrameShape(QFrame::NoFrame);
        graphicsView->setLineWidth(-1);
        graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        graphicsView->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        graphicsView->setRenderHints(QPainter::Antialiasing|QPainter::HighQualityAntialiasing|QPainter::NonCosmeticDefaultPen|QPainter::Qt4CompatiblePainting|QPainter::SmoothPixmapTransform|QPainter::TextAntialiasing);

        verticalLayout->addWidget(graphicsView);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        label->setText(QApplication::translate("MainWindow", "1. X v0", nullptr));
        label_11->setText(QApplication::translate("MainWindow", "2. mass", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "1. mass", nullptr));
        label_10->setText(QApplication::translate("MainWindow", "1. diameter", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "2.X v0", nullptr));
        label_12->setText(QApplication::translate("MainWindow", "2. diameter", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "1.Y v0", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "2.Y v0", nullptr));
        reset_btn->setText(QApplication::translate("MainWindow", "Reset", nullptr));
        startStop_btn->setText(QApplication::translate("MainWindow", "Start", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
