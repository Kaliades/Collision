#include "projection.h"
#include <stdexcept>
Projection::Projection()
{
     aSceneX =0;
     aSceneY=0;
     bSceneX=0;
     bSceneY=0;
     aRealX=0;
     aRealY=0;
     bRealX=0;
     bRealY=0;
}

void Projection::setUp(double sceenWidth, double sceenHigth, double realWidth, double realHight)
{
    valideData(sceenHigth);
    valideData(sceenWidth);
    valideData(realHight);
    valideData(realWidth);

    //Scene
    //X
    aSceneX = sceenWidth/realWidth;
    //bSceneX = 0;
    //Y
    aSceneY = -sceenHigth/realHight;
    bSceneY = sceenHigth;

    aRealX = 1.0/aSceneX;
    //bRealX = 0
    aRealY = 1.0/aSceneY;
    bRealY = -bSceneY/aSceneY;

}

Point2D Projection::getReal(double x, double y)
{
    return Point2D(
                aRealX*x+bRealX,
                aRealY*y+bRealY
                );
}

Point2D Projection::getScene(double x, double y)
{
       return Point2D(
                   aSceneX*x+bSceneX,
                   aSceneY*y+bSceneY
                   );
}
void Projection::valideData(double value)
{
    if(value<0)
        throw std::invalid_argument("Projection value is less then 0!");
}

