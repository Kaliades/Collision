#ifndef PROJECTION_H
#define PROJECTION_H

#include "physic/point2d.h"

class Projection
{
private:
    double aSceneX;
    double aSceneY;
    double bSceneX;
    double bSceneY;
    double aRealX;
    double aRealY;
    double bRealX;
    double bRealY;
    void valideData(double);
public:
    Projection();
    void setUp(double sceenWidth, double sceenHigth, double realWidth, double realHight);
    Point2D getReal(double, double);
    Point2D getScene(double, double);
};

#endif // PROJECTION_H
