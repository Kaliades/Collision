#ifndef PHYSICBALL_H
#define PHYSICBALL_H

#include "vectorofmountion.h"

class PhysicBall
{
private:
    VectorOfMountion *vectorOfMontion;

    int mass;
    double diameter;

public:
    PhysicBall(VectorOfMountion* , double, int mass = 10);
    ~PhysicBall();

    int getMass() const;
    void setMass(int value);
    double getDiameter() const;
    void setDiameter(double value);
    VectorOfMountion* getVectorOfMontion();
    void setVectorOfMontion(VectorOfMountion *value);
    void chagneDirectionX(double wall, double& time);
    void chagneDirectionY(double wall, double& time);
};

#endif // PHYSICBALL_H
