#ifndef POINT2D_H
#define POINT2D_H


class Point2D
{
private:
    double x;
    double y;
public:
    Point2D();
    Point2D(double, double);
    double getX() const;
    void setX(double value);
    double getY() const;
    void setY(double value);
};

#endif // POINT2D_H
