#include "point2d.h"

double Point2D::getX() const
{
    return x;
}

void Point2D::setX(double value)
{
    x = value;
}

double Point2D::getY() const
{
    return y;
}

void Point2D::setY(double value)
{
    y = value;
}

Point2D::Point2D()
{
    x = 0;
    y =0;
}

Point2D::Point2D(double x, double y)
{
    this->x = x;
    this->y = y;
}
