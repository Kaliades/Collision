#include "physiccalculator.h"
#include <math.h>
#include <cmath>

PhysicCalculator::PhysicCalculator(CommunicationScene *view,  PhysicBall *first, PhysicBall *second)
{
    this->view = view;
    firstBall = first;
    secondBall = second;
    firstBallTimeX = 0;
    firstBallTimeY = 0;
    secondBallTimeX = 0;
    secondBallTimeY = 0;
    deltaTime = 0.1;
}
PhysicCalculator::~PhysicCalculator(){
    delete  firstBall;
    delete secondBall;
}

void PhysicCalculator::start()
{

   Point2D firstScenePosition = getNewPostionOfFirstBall();
   Point2D secondScenePosition = getNewPostionOfSecondBall();
   if( ballsCollision(firstScenePosition, secondScenePosition)){
    firstScenePosition = getNewPostionOfFirstBall();
     secondScenePosition = getNewPostionOfSecondBall();
   }

    view->setFirstBallPostion(firstScenePosition);
    view->setSecondBallPostion(secondScenePosition);
}

Point2D PhysicCalculator::getNewPostionOfFirstBall()
{
    firstBallTimeX += deltaTime;
    firstBallTimeY += deltaTime;
    VectorOfMountion *vector = firstBall->getVectorOfMontion();
    Point2D position(vector->getXEquation()->getPostionByTime(firstBallTimeX),
                    vector->getYEquation()->getPostionByTime(firstBallTimeY));
    // X axis
    if(position.getX()+firstBall->getDiameter()>=view->getWidthOfScreen()||position.getX()+firstBall->getDiameter()<=0){
        firstBall->chagneDirectionX(view->getFirstBallCurrentPostion().getX(), firstBallTimeX);
        position.setX(vector->getXEquation()->getPostionByTime(firstBallTimeX));
    }
    // Y axis
    if(position.getY()+firstBall->getDiameter()>=view->getHightOfScreen() || position.getY()+firstBall->getDiameter()<0){
        firstBall->chagneDirectionY(view->getFirstBallCurrentPostion().getY(), firstBallTimeY);
        position.setY(vector->getYEquation()->getPostionByTime(firstBallTimeY));
    }

    return position;
}

Point2D PhysicCalculator::getNewPostionOfSecondBall()
{
    secondBallTimeX += deltaTime;
    secondBallTimeY += deltaTime;
    VectorOfMountion *vector = secondBall->getVectorOfMontion();
    Point2D position(vector->getXEquation()->getPostionByTime(secondBallTimeX),
                    vector->getYEquation()->getPostionByTime(secondBallTimeY));
      // X axis
    if(position.getX()+secondBall->getDiameter()>=view->getWidthOfScreen()||position.getX()+secondBall->getDiameter()<=0){
        secondBall->chagneDirectionX(view->getSecondBallCurrentPostion().getX(), secondBallTimeX);
        position.setX(vector->getXEquation()->getPostionByTime(secondBallTimeX));
    }
    // Y axis
    if(position.getY()+secondBall->getDiameter()>=view->getHightOfScreen() || position.getY()+secondBall->getDiameter()<0){
        secondBall->chagneDirectionY(view->getSecondBallCurrentPostion().getY(), secondBallTimeY);
        position.setY(vector->getYEquation()->getPostionByTime(secondBallTimeY));
    }
    return position;
}

bool PhysicCalculator::ballsCollision(Point2D firstPosition, Point2D secondPosition)
{
    bool value;
    double x = firstPosition.getX()-secondPosition.getX();
    double y = firstPosition.getY()-secondPosition.getY();
    double c = sqrt(x*x+y*y);
    value = c < firstBall->getDiameter()/2+secondBall->getDiameter()/2;

    if(value)
    {

        double fi  = acos(abs(x)/(c*1.0));
        calculateCollision(fi);
    }

    return value;
}

void PhysicCalculator::calculateCollision(double fi)
{
    //http://williamecraver.wixsite.com/elastic-equations?fbclid=IwAR1Hx0SwBXzgW6Xq9Z_st8pszWdYfZoiLjIm39CCLC4A2bKEKlvchQBVow8
    double teta1 = firstBall->getVectorOfMontion()->getAngleBetweenVelocityAndVelocityX(firstBallTimeX, firstBallTimeY);
    double teta2 = secondBall->getVectorOfMontion()->getAngleBetweenVelocityAndVelocityX(secondBallTimeX, secondBallTimeY);
    double speedFirst = firstBall->getVectorOfMontion()->getSpeed(firstBallTimeX, firstBallTimeY);
    double speedSecond = secondBall->getVectorOfMontion()->getSpeed(secondBallTimeX, secondBallTimeY);
    int mass1 = firstBall->getMass();
    int mass2 = secondBall->getMass();
    double cosTeta1MinusFi = cos(teta1-fi);
    double cosTeta2MinusFi = cos(teta2-fi);
    double cosFi = cos(fi);
    double sinTeta1MinusFi = sin(teta1-fi);
    double cosFiPlusPi2 = cos(fi+M_PI/2.0);
    double sinFi = sin(fi);
    double sinFiPlusPi2 = sin(fi+M_PI/2.0);
    double sinTeta2MinusFi = sin(teta2-fi);
    double newFirstVelocityX = ((speedFirst*cosTeta1MinusFi*(mass1-mass2)+2.0*mass2*speedSecond*cosTeta2MinusFi)/(mass1+mass2))*cosFi+speedFirst*sinTeta1MinusFi*cosFiPlusPi2;
    double newFirstVelocityY = ((speedFirst*cosTeta1MinusFi*(mass1-mass1)+2.0*mass2*speedSecond*cosTeta2MinusFi)/(mass1+mass2))*sinFi+speedFirst*sinTeta1MinusFi*sinFiPlusPi2;
    double newSecondVelocityX = ((speedSecond*cosTeta2MinusFi*(mass2-mass1)+2.0*mass1*speedFirst*cosTeta1MinusFi)/(mass1+mass2))*cosFi+speedSecond*sinTeta2MinusFi*cosFiPlusPi2;
    double newSecondVelocityY = ((speedSecond*cosTeta2MinusFi*(mass2-mass1)+2.0*mass1*speedFirst*cosTeta1MinusFi)/(mass1+mass2))*sinFi+speedSecond*sinTeta2MinusFi*sinFiPlusPi2;
    VectorOfMountion *first = firstBall->getVectorOfMontion();
    first->getXEquation()->setPostionZero(view->getFirstBallCurrentPostion().getX());
    first->getXEquation()->setVelocityZero(newFirstVelocityX);
    first->getYEquation()->setPostionZero(view->getFirstBallCurrentPostion().getY());
    first->getYEquation()->setVelocityZero(newFirstVelocityY);
    firstBallTimeX = 0;
    firstBallTimeY = 0;
    VectorOfMountion *second = secondBall->getVectorOfMontion();
    second->getXEquation()->setPostionZero(view->getSecondBallCurrentPostion().getX());
    second->getXEquation()->setVelocityZero(newSecondVelocityX);
    second->getYEquation()->setPostionZero(view->getSecondBallCurrentPostion().getY());
    second->getYEquation()->setVelocityZero(newSecondVelocityY);
    secondBallTimeX = 0;
    secondBallTimeY = 0;

}

