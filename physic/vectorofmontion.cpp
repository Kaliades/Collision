#include "vectorofmountion.h"
#include <cmath>

VectorOfMountion::EquationOfMontion::EquationOfMontion(double x0, double v0, double a){
    postionZero = x0;
    velocityZero = v0;
    acceleration = a;
}
double VectorOfMountion::EquationOfMontion::getVelocityZero() const
{
    return velocityZero;
}

void VectorOfMountion::EquationOfMontion::setVelocityZero(double value)
{
    velocityZero = value;
}


double VectorOfMountion::EquationOfMontion::getAcceleration() const
{
    return acceleration;
}

void VectorOfMountion::EquationOfMontion::setAcceleration(double value)
{
    acceleration = value;
}

double VectorOfMountion::EquationOfMontion::getPostionZero() const
{
    return postionZero;
}

void VectorOfMountion::EquationOfMontion::setPostionZero(double value)
{
    postionZero = value;
}

double VectorOfMountion::EquationOfMontion::getPostionByTime(double t){
    return postionZero+velocityZero*t+acceleration*t*t/2.0;
}

double VectorOfMountion::EquationOfMontion::getVelocityByTime(double t){
    return velocityZero+acceleration*t;
}

void VectorOfMountion::EquationOfMontion::chagneVelocityDirection()
{
    acceleration *=-1;
    velocityZero *=-1;
}

VectorOfMountion::VectorOfMountion()
{
  vector2d.at(X) = nullptr;
  vector2d.at(Y) = nullptr;
}

VectorOfMountion::~VectorOfMountion()
{
  for(EquationOfMontion* x : vector2d)
      if(x!=nullptr)
        delete  x;
}

void VectorOfMountion::setXEquation(VectorOfMountion::EquationOfMontion *x)
{
    if(vector2d.at(X)!=nullptr)
        delete vector2d.at(X);
    vector2d.at(X) =x;
}

void VectorOfMountion::setYEquation(VectorOfMountion::EquationOfMontion *y)
{
    if(vector2d.at(Y)!=nullptr)
        delete vector2d.at(Y);
    vector2d.at(Y) =y;
}

VectorOfMountion::EquationOfMontion* VectorOfMountion::getXEquation()
{
    return vector2d.at(X);
}

VectorOfMountion::EquationOfMontion* VectorOfMountion::getYEquation()
{
    return  vector2d.at(Y);
}

double VectorOfMountion::getAngleBetweenVelocityAndVelocityX(double timeX, double timeY)
{
    double alpha = 0;
    if(vector2d.at(X)->getVelocityByTime(timeX)<0 || vector2d.at(Y)->getVelocityByTime(timeY)<0)
        alpha = M_PI;
    double velocityXSquare = vector2d.at(X)->getVelocityByTime(timeX)*vector2d.at(X)->getVelocityByTime(timeX);
    double velocityY = vector2d.at(Y)->getVelocityByTime(timeY);
    double speed = sqrt(velocityXSquare+velocityY*velocityY);
    if(speed==0.0)
        return M_PI/4;
    return acos((sqrt(velocityXSquare)/speed))+alpha;
}

double VectorOfMountion::getSpeed(double timeX, double timeY)
{
    double velocityX = vector2d.at(X)->getVelocityByTime(timeX);
    double velocityY = vector2d.at(Y)->getVelocityByTime(timeY);
    return sqrt(velocityX*velocityX+velocityY*velocityY);
}

