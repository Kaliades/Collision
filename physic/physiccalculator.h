#ifndef PHYSICCALCULATOR_H
#define PHYSICCALCULATOR_H

#include <QDebug>
#include "communication/communicationscene.h"
#include "physicball.h"


class PhysicCalculator
{
private:
    CommunicationScene *view;
    PhysicBall *firstBall;
    PhysicBall *secondBall;
    double firstBallTimeX;
    double firstBallTimeY;
    double secondBallTimeX;
    double secondBallTimeY;
    double deltaTime;
    Point2D getNewPostionOfFirstBall();
    Point2D getNewPostionOfSecondBall();
    bool ballsCollision(Point2D firstPosition, Point2D secondPosition);
    void calculateCollision(double fi);
public:
    PhysicCalculator(CommunicationScene *, PhysicBall* first, PhysicBall* second);
    ~PhysicCalculator();
    void start();

};

#endif // PHYSICCALCULATOR_H
