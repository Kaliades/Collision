#include "physicball.h"

PhysicBall::PhysicBall(VectorOfMountion *vector, double diameter, int mass)
{
    this->vectorOfMontion = vector;
    this->mass = mass;
    this->diameter = diameter;
}

PhysicBall::~PhysicBall()
{
    delete vectorOfMontion;
}

int PhysicBall::getMass() const
{
    return mass;
}

void PhysicBall::setMass(int value)
{
    mass = value;
}

double PhysicBall::getDiameter() const
{
    return diameter;
}

void PhysicBall::setDiameter(double value)
{
    diameter = value;
}

VectorOfMountion* PhysicBall::getVectorOfMontion()
{
    return vectorOfMontion;
}

void PhysicBall::setVectorOfMontion(VectorOfMountion *value)
{
    vectorOfMontion = value;
}

void PhysicBall::chagneDirectionX(double wall, double &time)
{
    double currentVelocity = vectorOfMontion->getXEquation()->getVelocityByTime(time);
    vectorOfMontion->getXEquation()->setPostionZero(wall);
    vectorOfMontion->getXEquation()->setVelocityZero(currentVelocity);
    vectorOfMontion->getXEquation()->chagneVelocityDirection();
    time = 0;

}

void PhysicBall::chagneDirectionY(double wall, double &time)
{
    double currentVelocity = vectorOfMontion->getYEquation()->getVelocityByTime(time);
    vectorOfMontion->getYEquation()->setPostionZero(wall);
    vectorOfMontion->getYEquation()->setVelocityZero(currentVelocity);
    vectorOfMontion->getYEquation()->chagneVelocityDirection();
    time = 0;
}

