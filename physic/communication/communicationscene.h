#ifndef COMMUNICATIONSCENE_H
#define COMMUNICATIONSCENE_H

#include "physic/point2d.h"

class CommunicationScene
{
public:
    CommunicationScene();
    virtual ~CommunicationScene();
    virtual double getWidthOfScreen()=0;
    virtual double getHightOfScreen()=0;
    virtual Point2D getFirstBallCurrentPostion()=0;
    virtual Point2D getSecondBallCurrentPostion()=0;
    virtual void setFirstBallPostion(Point2D)= 0;
    virtual void setSecondBallPostion(Point2D) = 0;
    virtual int getDiameterFirstBall() = 0;
    virtual int getDiameterSecondBall() = 0;
};


#endif // COMMUNICATIONSCENE_H
