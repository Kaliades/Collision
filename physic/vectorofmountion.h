#ifndef VECTOROFMONTION_H
#define VECTOROFMONTION_H


#include <array>

#define X 0
#define Y 1
#define DIMENSION 2

class VectorOfMountion
{
public:
    class EquationOfMontion{
    private:
        double postionZero;
        double velocityZero;
        double acceleration;
    public:
        EquationOfMontion(double x0, double v0, double a);
        double getPostionByTime(double t);
        double getVelocityByTime(double t);
        double getAcceleration() const;
        void setAcceleration(double value);
        void chagneVelocityDirection();
        double getPostionZero() const;
        void setPostionZero(double value);
        double getVelocityZero() const;
        void setVelocityZero(double value);
    };
private:
    std::array <EquationOfMontion*, DIMENSION> vector2d;
public:
    VectorOfMountion();
    ~VectorOfMountion();
    void setXEquation(EquationOfMontion*);
    void setYEquation(EquationOfMontion*);
    EquationOfMontion* getXEquation();
    EquationOfMontion* getYEquation();
    double getAngleBetweenVelocityAndVelocityX(double timeX, double timeY);
    double getSpeed(double timeX, double timeY);

};

#endif // VECTOROFMONTION_H
