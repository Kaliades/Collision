#ifndef EQUATIONOFMONTION_H
#define EQUATIONOFMONTION_H


class EquationOfMontion
{
private:
    double postionZero;
    double velocityZero;
    double acceleration;
public:
    EquationOfMontion(double x0, double v0, double a);
    double getPostionByTime(double t);
    double getVelocityByTime(double t);

};

#endif // EQUATIONOFMONTION_H
