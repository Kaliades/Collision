﻿#include "myscene.h"

#include <QDebug>
#include <qmath.h>
MyScene::MyScene(QGraphicsView &qView, int diameterF, int diameterS)
    :QGraphicsScene (qView.sceneRect())
{  
    firstBall = new ViewBall(QColor(150,150,152), diameterF);
    secondBall = new ViewBall(QColor(80,90,69),diameterS);
    this->addItem(firstBall);
    this->addItem(secondBall);
    firstBall->setPos(250, 250);
    secondBall->setPos(500, 500);
    followFirstBall = false;
    followSecondBall = false;
}
MyScene::~MyScene(){
    delete firstBall;
    delete secondBall;

}

void MyScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    double sceneX = event->scenePos().x();
    double sceneY = event->scenePos().y();
    //first ball
    double ballX = firstBall->pos().x();
    double ballY = firstBall->pos().y();
    if(abs(sceneX-ballX)<firstBall->getDiameter()-10 &&
            abs(sceneY-ballY)<firstBall->getDiameter()-10){
        followFirstBall = true;
        return;
    }
    //second ball
    ballX = secondBall->pos().x();
    ballY = secondBall->pos().y();
    if(abs(sceneX-ballX)<secondBall->getDiameter()-10 &&
            abs(sceneY-ballY)<secondBall->getDiameter()-10){
        followSecondBall = true;
        return;
    }

}

void MyScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    if(followFirstBall){
        firstBall->setPos(mouseEvent->scenePos().x(), mouseEvent->scenePos().y());
        followFirstBall = false;
    }
    if(followSecondBall){
        secondBall->setPos(mouseEvent->scenePos().x(), mouseEvent->scenePos().y());
        followSecondBall = false;
    }
}

// impl MovingBall
double MyScene::getWidthOfScreen()
{
    return this->width();
}

double MyScene::getHightOfScreen()
{
    return this->height();
}

Point2D MyScene::getFirstBallCurrentPostion()
{
    return Point2D(firstBall->pos().x(), firstBall->pos().y());
}

Point2D MyScene::getSecondBallCurrentPostion()
{
    return Point2D(secondBall->pos().x(), secondBall->pos().y());
}

void MyScene::setFirstBallPostion(Point2D pos)
{
    firstBall->setX(pos.getX());
    firstBall->setY(pos.getY());
}

void MyScene::setSecondBallPostion(Point2D pos)
{
    secondBall->setX(pos.getX());
    secondBall->setY(pos.getY());
}

int MyScene::getDiameterFirstBall()
{
    return firstBall->getDiameter();
}

int MyScene::getDiameterSecondBall()
{
    return  secondBall->getDiameter();
}





