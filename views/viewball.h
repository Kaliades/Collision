#ifndef VIEWBALL_H
#define VIEWBALL_H

#include <QGraphicsEllipseItem>
#include <QColor>
class ViewBall:public QGraphicsEllipseItem
{
private:
    int diameter;
public:
    ViewBall(QColor, int );

    int getDiameter() const;
};

#endif // VIEWBALL_H
