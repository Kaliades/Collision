#include "viewball.h"
#include<QBrush>
#include<QPen>
int ViewBall::getDiameter() const
{
    return diameter;
}

ViewBall::ViewBall(QColor color, int diameter)
    :QGraphicsEllipseItem ()
{
    setBrush(QBrush(color));
    setPen(QPen(color));
    this->diameter = diameter;
    setRect(0,0,diameter, diameter);
}
