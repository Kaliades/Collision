#ifndef MYSCENE_H
#define MYSCENE_H

#include<QGraphicsScene>
#include <QGraphicsView>
#include "viewball.h"
#include <QGraphicsSceneMouseEvent>
#include <QPointF>
#include "physic/communication/communicationscene.h"
class MyScene: public QGraphicsScene, public CommunicationScene
{
private:
    ViewBall *firstBall;
    ViewBall *secondBall;
    bool followFirstBall;
    bool followSecondBall;
public:
    MyScene(QGraphicsView &, int diameterF, int diameterS);
   ~MyScene();
    // impl CommunicationScene
    double getWidthOfScreen();
    double getHightOfScreen();
    Point2D getFirstBallCurrentPostion();
    Point2D getSecondBallCurrentPostion();
    void setFirstBallPostion(Point2D);
    void setSecondBallPostion(Point2D);
    int getDiameterFirstBall();
    int getDiameterSecondBall();
public slots:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent);
};

#endif // MYSCENE_H
