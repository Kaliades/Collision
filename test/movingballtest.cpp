#include "movingballtest.h"

MovingBallTest::MovingBallTest(CommunicationScene *c)
{
    this->ballController = c;
    double s = 15;
    fStepX = s;
    fStepY = s;
    sStepX = s;
    sStepY = s;
}

void MovingBallTest::start()
{
    //first ball
    double postionX = ballController->getFirstBallCurrentPostion().getX()+fStepX;
    double postionY = ballController->getFirstBallCurrentPostion().getY()+fStepY;
    //axis X
    if(postionX > ballController->getWidthOfScreen() ||
            postionX < 0)
        fStepX *= -1;
    //axis Y
    if(postionY > ballController->getHightOfScreen() ||
            postionY < 0)
        fStepY *= -1;
    ballController->setFirstBallPostion(Point2D(postionX, postionY));
     //second ball
    postionX = ballController->getSecondBallCurrentPostion().getX()+sStepX;
    postionY = ballController->getSecondBallCurrentPostion().getY()+sStepY;
    if(postionX > ballController->getWidthOfScreen() ||
            postionX < 0)
        sStepX *= -1;
    //axis Y
    if(postionY > ballController->getHightOfScreen() ||
            postionY < 0)
        sStepY *= -1;
    ballController->setSecondBallPostion(Point2D(postionX,postionY));
}

