#ifndef MOVINGBALLTEST_H
#define MOVINGBALLTEST_H

#include "physic/communication/communicationscene.h"

class MovingBallTest
{
private:
    CommunicationScene* ballController;
    double fStepX;
    double fStepY;
    double sStepX;
    double sStepY;
public:
    MovingBallTest(CommunicationScene*);
    void start();

};

#endif // MOVINGBALLTEST_H
