#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "views/myscene.h"
#include <QTimer>
#include <physic/physiccalculator.h>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void init();

private:
    PhysicCalculator *calculator;
    Ui::MainWindow *ui;
    MyScene *scene;
    QTimer* timer;
    bool areBallsMoving;
    void initData();
private slots:
    void onStartStopBtn();
    void startStopMoveBall();
    void resetBtn();

};

#endif // MAINWINDOW_H
